abstract class Parser<in TInput, TOutput> {
    protected abstract fun parse(value: TInput): TOutput

    @Throws(ParsingError::class)
    infix fun unsafeParse(value: TInput): TOutput = parse(value)
    infix fun tryParse(value: TInput): ValueError<TOutput> = ValueError { parse(value) }

    operator fun invoke(value: TInput) = tryParse(value)

    companion object {
        inline infix fun <TIn, TOut> byCustom(crossinline parser: (TIn) -> TOut) = object : Parser<TIn, TOut>() {
            override fun parse(value: TIn): TOut = parser(value)
        }
    }
}